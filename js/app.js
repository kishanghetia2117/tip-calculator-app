let billAmt = document.getElementById('bill-amount');
let tipAmt = document.getElementById('tip-amt');
let tipCheckBox = document.querySelectorAll('.tip-checkbox')
let count = document.getElementById('people-count');
const totalAmt = document.querySelectorAll('.value');
const resetBtn = document.querySelector('.reset');
const countError = document.querySelector('.people-error-msg');

billAmt.addEventListener('input', totalBill);
tipCheckBox.forEach(tipCheckBox => {
  tipCheckBox.addEventListener('change', handleClick);
});
tipAmt.addEventListener('click', clearCheckBox);
tipAmt.addEventListener('input', customTip);
count.addEventListener('input', billPerPerson);
resetBtn.addEventListener('click', reset);

let billValue = 0.0;
let tipValue = 0.15;
let peopleCount = 1;

function totalBill() {
  disableEnableReset();
  billValue = parseFloat(billAmt.value);
  calculateTip();
}

function handleClick() {
  console.clear()
  tipAmt.value = ''
  tipCheckBox.forEach((box) => {
    if (box !== this) box.checked = false
  })
  let enabledSettings = Array.from(tipCheckBox)
    .filter(i => i.checked)
    .map(i => i.value)

  if (enabledSettings[0] > 0) {
    tipValue = enabledSettings[0]
    calculateTip();
  } else {
    tipValue = 0
    calculateTip();
  }
}

function clearCheckBox() {
  tipCheckBox.forEach((box) => {
    box.checked = false
  })
  tipValue = tipAmt.value / 100;
  calculateTip();
}
function customTip() {
  tipValue = parseFloat(tipAmt.value / 100);
  calculateTip();
}

function billPerPerson() {
  peopleCount = parseFloat(count.value);
  if (peopleCount > 0) {
    calculateTip();
    hideErr(countError)
  } else if (count.value == '') {
    console.log("reste count  to 1 ")
    peopleCount = 1
    calculateTip();
    hideErr(countError)
  } else {
    displaErr(countError)
    calculateTip();
  }
}
function displaErr(ele) {
  ele.style.display = "initial"
}
function hideErr(ele) {
  ele.style.display = "none"
}
function resetAns() {
  totalAmt[0].innerHTML = '$' + '0.00';
  totalAmt[1].innerHTML = '$' + '0.00';
}
function calculateTip() {
  if (billValue > 0 && tipValue >= 0 && peopleCount > 0 && Number.isInteger(peopleCount)) {
    let tipAmount = billValue * tipValue / peopleCount;
    let total = (billValue / peopleCount) + tipAmount;
    console.log(billValue, tipValue, peopleCount, tipAmount, total)
    // if (isNaN(tipAmount) || isNaN(total) ) {
    // if (tipAmount < 0 || total < 0 || Math.sign(tipAmount) != 1 || Math.sign(total) != 1 || peopleCount < 1) {
    //   resetAns()
    // } else {
    totalAmt[0].innerHTML = '$' + tipAmount.toFixed(2);
    totalAmt[1].innerHTML = '$' + total.toFixed(2);
    // }
  } else {
    resetAns()
  }
}
function disableEnableReset() {
  console.log(billAmt.value)
  if (billAmt.value == '') {
    resetBtn.disabled = true;
    resetBtn.style.opacity = '0.3';
  }
  else {
    resetBtn.disabled = false;
    resetBtn.style.opacity = '1';
  }
}

function reset() {
  billValue = 0.0
  tipValue = 0.10
  billAmt.value = ''
  tipAmt.value = ''
  count.value = ''
  calculateTip();
  disableEnableReset();
}